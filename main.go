package main

import (
	"bufio"
	"errors"
	"fmt"
	. "github.com/logrusorgru/aurora"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

// Node is a single field in the Space. It can be mined,
// it holds a number of mined neighbors, it can be flagged
// or revealed by the player.
type Node struct {
	Mined     bool
	Flagged   bool
	Revealed  bool
	Neighbors int
}

func (n Node) String() string {
	if n.Revealed {
		if n.Mined {
			return Sprintf(BrightRed("●").Bold())
		} else if n.Neighbors == 0 {
			return " "
		} else {
			nbs := n.Neighbors
			if nbs == 1 {
				return Sprintf(BrightBlue("1").Bold())
			} else if nbs == 2 {
				return Sprintf(Green("2").Bold())
			} else if nbs == 3 {
				return Sprintf(Yellow("3").Bold())
			} else {
				return Sprintf(Red(nbs).Bold())
			}
		}
	} else if n.Flagged {
		return Sprintf(Red("○").Bold()) //●
	} else {
		return Sprintf(Gray(12-1, "?"))
	}
}

// ---------------------------------------------

// Space is a collection of Nodes arranged as a 2D table.
type Space struct {
	Nodes [][]Node
}

func (s Space) String() string {
	var repr string

	repr += Sprintf(Gray(8-1, "\n┌────┬"))
	for j := 0; j < len(s.Nodes[0])-1; j++ {
		repr += Sprintf(Gray(8-1, "────┬"))
	}
	repr += Sprintf(Gray(8-1, "────┐\n"))

	repr += Sprintf(Gray(8-1, "│    │"))
	for j := range s.Nodes[0] {
		repr += Sprintf(Gray(8-1, fmt.Sprintf("%3d │", j)))
	}
	repr += Sprintf(Gray(8-1, "\n├────┼"))
	for j := 0; j < len(s.Nodes[0])-1; j++ {
		repr += Sprintf(Gray(8-1, "────┼"))
	}
	repr += Sprintf(Gray(8-1, "────┤\n"))

	for i, row := range s.Nodes {
		repr += Sprintf(Gray(8-1, fmt.Sprintf("│%3d │", i)))
		for _, n := range row {
			repr += fmt.Sprintf("  %s %s", n, Sprintf(Gray(8-1, "│")))
		}
		repr += Sprintf(Gray(8-1, "\n├────┼"))
		for j := 0; j < len(s.Nodes[0])-1; j++ {
			repr += Sprintf(Gray(8-1, "────┼"))
		}
		repr += Sprintf(Gray(8-1, "────┤\n"))
	}

	repr += Sprintf(Gray(8-1, "│    │"))
	for j := range s.Nodes[0] {
		repr += Sprintf(Gray(8-1, fmt.Sprintf("%3d │", j)))
	}
	repr += Sprintf(Gray(8-1, "\n└────┴"))
	for j := 0; j < len(s.Nodes[0])-1; j++ {
		repr += Sprintf(Gray(8-1, "────┴"))
	}
	repr += Sprintf(Gray(8-1, "────┘\n"))

	return repr
}

// Dim returns the dimensions of the Space
func (s Space) Dim() []int {
	var dim []int
	if len(s.Nodes) == 0 {
		dim = []int{0, 0}
	} else {
		dim = []int{len(s.Nodes), len(s.Nodes[0])}
	}
	return dim
}

// FindNeighbors calculates and returns the coordinates
// of all the neighboring Nodes to a Node with given
// coordinates.
func (s *Space) FindNeighbors(i, j int) [][]int {
	dim := s.Dim()
	potentialNeighbors := [][]int{
		[]int{i - 1, j - 1},
		[]int{i - 1, j},
		[]int{i - 1, j + 1},
		[]int{i, j - 1},
		[]int{i, j + 1},
		[]int{i + 1, j - 1},
		[]int{i + 1, j},
		[]int{i + 1, j + 1},
	}
	neighbors := make([][]int, 0)

	for _, ns := range potentialNeighbors {
		if ns[0] >= 0 && ns[0] < dim[0] &&
			ns[1] >= 0 && ns[1] < dim[1] {
			neighbors = append(neighbors, []int{ns[0], ns[1]})
		}
	}
	return neighbors
}

// Build a new Space, creating all the Nodes and randomly
// mining some of them.
func (s *Space) Build(d1, d2 int) {

	// Populate the space
	s.Nodes = make([][]Node, d1)
	for i := 0; i < d1; i++ {
		s.Nodes[i] = make([]Node, d2)
		for j := 0; j < d2; j++ {
			s.Nodes[i][j] = Node{
				Mined:     false,
				Flagged:   false,
				Revealed:  false,
				Neighbors: 0,
			}
		}
	}
	// Lay mines and update neighbor counts
	rand.Seed(time.Now().UnixNano())
	// this is practically the difficulty level
	chance := 0.15
	for i, row := range s.Nodes {
		for j, _ := range row {
			// randomly decide if there should be a mine
			// given the overall chance
			mined := rand.Float64() < chance
			s.Nodes[i][j].Mined = mined
			// update neighbors
			if mined {
				for _, ns := range s.FindNeighbors(i, j) {
					s.Nodes[ns[0]][ns[1]].Neighbors++
				}
			}
		}
	}
}

// Reveal a single field, betting that it is not mined.
func (s *Space) Reveal(y, x int) {
	dim := s.Dim()
	if y < dim[0] && x < dim[1] {
		s.Nodes[y][x].Revealed = true
	}
	if s.Nodes[y][x].Neighbors == 0 {
		for _, ns := range s.FindNeighbors(y, x) {
			if !s.Nodes[ns[0]][ns[1]].Revealed {
				s.Reveal(ns[0], ns[1])
			}
		}

	}
}

// Flag a single field, betting that it is mined.
func (s *Space) Flag(y, x int) {
	dim := s.Dim()
	if y < dim[0] && x < dim[1] {
		flagged := s.Nodes[y][x].Flagged
		s.Nodes[y][x].Flagged = !flagged
	}
}

// CheckVictory returns true if the player won, false otherwise.
// Winning condition is: there is no field that would not
// be either mined or revealed.
func (s *Space) CheckVictory() bool {
	for i, row := range s.Nodes {
		for j := range row {
			if !s.Nodes[i][j].Revealed && !s.Nodes[i][j].Mined {
				return false
			}
		}
	}
	return true
}

// CheckGameover returns true if the chosen field is mined.
func (s *Space) CheckGameover(v1, v2 int) bool {
	return s.Nodes[v1][v2].Mined
}

// ----------------------------------------------------------

func win() {
	fmt.Println()
	fmt.Println("Congratulations! You won the game!")
}

func gameover() {
	fmt.Println()
	fmt.Println("You exploded! Game over!")
}

// readCmd reads user keyboard input and parses the commands.
func readCmd(reader *bufio.Reader) (string, int, int, error) {
	fmt.Print(Yellow("⟫ ").Bold())
	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	text = strings.TrimSpace(text)
	splits := strings.Split(text, " ")
	if len(splits) != 3 {
		return "", -1, -1, errors.New("3 parameters are required: CMD V1 V2")
	}
	cmd := splits[0]
	v1, err := strconv.Atoi(splits[1])
	v2, err := strconv.Atoi(splits[2])
	return cmd, v1, v2, err
}

func validateCoordinates(space Space, cmd string, y, x int) bool {
	if cmd != "build" {
		if space.Nodes == nil {
			fmt.Println("Board not initialized! Use build Y X first.")
			fmt.Println()
			return false
		}
		dim := space.Dim()
		if y < 0 || y > dim[0] || x < 0 || x > dim[1] {
			fmt.Println("Numerical parameters out of bounds of the game board!")
			fmt.Println()
			return false
		}
	}
	if y < 0 || x < 0 {
		fmt.Println("Numerical parameters must be positive!")
		fmt.Println()
		return false
	}

	return true
}

func main() {

	fmt.Println()
	fmt.Println("Welcome to " + Sprintf(Red("xmswp").Bold()) + ", a very nice command line minesweeper clone.")
	fmt.Println()
	fmt.Println("You will use a few simple commands to play this game.")
	fmt.Println("Start with " + Sprintf(Yellow("build X Y").Bold()) + " (for example " + Sprintf(Yellow("build 10 10").Bold()) + ") to generate a board.")
	fmt.Println("Then you can use the following commands:")
	fmt.Println()
	fmt.Println(Sprintf(Yellow("check X Y").Bold()) + " (or just " + Sprintf(Yellow("c X Y").Bold()) + ") to check a field")
	fmt.Println(Sprintf(Yellow("flag X Y").Bold()) + " (or just " + Sprintf(Yellow("f X Y").Bold()) + ") to flag a field")
	fmt.Println()
	fmt.Println("The goal is, as always, to reveal all the fields without mines.")
	fmt.Println("Good luck!")

	reader := bufio.NewReader(os.Stdin)
	var space Space

	fmt.Println()

	// Main command loop.
	//
	// Every command needs to be of the form:
	//   COMMAND NUM1 NUM2
	// Where COMMAND is a string and NUM1/NUM2 are integers.
	for {
		cmd, v1, v2, err := readCmd(reader)
		if err != nil {
			fmt.Println("You need to provide three arguments: command val1 val2.")
			fmt.Println()
			continue
		}
		if !validateCoordinates(space, cmd, v1, v2) {
			continue
		}

		// shortcut aliases
		if cmd == "b" {
			cmd = "build"
		} else if cmd == "c" {
			cmd = "check"
		} else if cmd == "f" {
			cmd = "flag"
		}

		switch cmd {
		case "build":
			// create a new board with given dimensions
			space = Space{}
			space.Build(v1, v2)
		case "check":
			// check a field
			space.Reveal(v1, v2)
			if space.CheckGameover(v1, v2) {
				gameover()
				break
			}
			if space.CheckVictory() {
				win()
				break
			}
		case "flag":
			// flag a field
			space.Flag(v1, v2)
		default:
			fmt.Println(
				"\nValid commands are: build, check, flag; ",
				"followed by two number, e.g.: build 10 10.",
			)
			fmt.Println()
			continue
		}
		if len(space.Nodes) != 0 {
			fmt.Println(space)
		}
	}
}
