# xmswp

A simple CLI minesweeper clone written in Go.

It works just like the original minesweeper, except that it runs in a terminal, and you use commands instead of clicking.

Initialize the board with a desired size with:

```build 10 14```

After that, you can check fields with:

```check Y X```

and flag them with:

```flag Y X```

Shortcut commands (`b`, `c`, `f`) will also work.

![xmswp](screenshot.png "xmswp screenshot")
